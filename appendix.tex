\documentclass[letter,sigplan,screen,sigconf,nonacm]{acmart}

\usepackage{listings}
\usepackage{acronym}
\usepackage{todonotes}
\usepackage{lstautogobble}
\usepackage{pdfpages}
\usepackage[caption=false]{subfig}


\acrodef{HDL}{Hardware Description Language}

\settopmatter{printfolios=false,printacmref=false}
\bibliographystyle{ACM-Reference-Format}


\newcommand{\todoi}[2][=1]{\todo[inline]{#2}}

% \setcopyright{rightsretained}
% \acmDOI{}
% \acmISBN{}
% \acmConference[LATTE '23]{3nd Workshop on Languages, Tools, and Techniques for Accelerator Design}{March 26, 2023}{Vancouver, BC, Canada}

\title{Appendix A: An actual paper}


\begin{document}

\includepdf[pages=-]{main.pdf}

\maketitle

\section{Background}

Paper minimization, the act of writing the shortest possible paper, is a
subject with a long history of incremental improvements\cite{Qiu2018,Lin2019,Jones2019,Bach2019,Wardin2019}. Recently, researchers
have also started studying paper maximization\cite{Abrams2021}, writing the longest possible paper
that can still get published. However, to date as far as the authors are aware,
no previous research investigates simultaneous maximization and minimization.

The key observation enabling our proposed method is that references are often
not counted towards the length of papers, with several conferences and journal
allowing extra space for references outside the normal page limit. In such
settings, the proposed method produces papers which are both minimal,
consisting only of a single short sentence in the body and maximal in the
number of pages.

\section{Method}

In the interest of reproducibility\footnote{And also to subject everyone to the
cursed code which contains 100 lines of chained and nested iterator functions} in science, the
tool that was developed to generate the above reference list is open source and
available for download\footnote{\url{https://gitlab.com/TheZoq2/sigbovik2023}}. The tool downloads a list of
thousands of papers published to Arxiv, and then generates a formatted
reference list based on a text file containing paper content.



\section{Results}

As can be seen from the proof of concept, the body of the paper is 6 words or
39 characters long. Crucially, this does not change with the information
content of the paper, which means that the non-references list information
complexity per paper size of our method is $O(1)$.

For the purposes of maximization, our method uses around two lines of paper
content per character of paper content. Comparing this to the state of the art
in paper maximization is difficult, most previously proposed methods inject
content via citation format expansion. Citation format expansion paper size
is $O(nc)$ for $c$ citations of length $n$, whereas our method grows by
$O(m\bar{c})$ for a paper with $\bar{c}$ characters and citation meta-data of
length $m$.

There are several reasons to prefer our method. Finding relevant
references\footnote{Ignore the fact that we use random papers dumped from
arxiv. Hopefully readers are lazy and don't actually look at the references} is tedious, at least more so than simply writing random
text and having a tool expand the text to take up more space on the page. For
example, one can make heavy use of examples to exemplify proposed methods and
claims.

\section{The Reference List Side Channel}

The reference list side channel exploited for paper maximization can also serve
other purposes, primarily injecting more content in papers for submissions
where references are not counted towards the page limit. In the present work,
this is of little use unless references are completely unbounded, due to the
low information content per page area. For example, this paper used only 2
sentences to fill 3 pages.

In order to properly exploit this side channel, more work to increase the
information content is required. For example, one might use the first word of
paper titles as the information deliverable. However, one has to be careful not
to devise a too complex scheme, as that risks using more space for the
description of how to read the injected information, than is actually delivered
via the injection.

\section{Future work}

This work serves as a proof of concept, however, some issues remain. The main
issue here is the formatting of the reference list. Some particularly picky
publishers may object to using vertical space to mark sentences, leading to a
rejection and undoubtedly, sadness. In order to mitigate this, one might
exploit the fact that citations of multiple papers with the same authors
replaces the author name with \textit{--} in some reference styles.

Another issue is the lack of special characters, injecting something like an
equation or actual citations require special characters such as [ and
$\oplus$. While it may be possible to find a select few papers published by
people whose names start with those characters, finding enough to write a
rigorous mathematical and well referenced paper may prove difficult.

\section{Conclusion}

We present a novel method to synthesise papers which are simultaneous maximal
and minimal. To do so, we exploit a previously unexplored avenue for paper
information injection via a side channel attack on the reference list. The
proposed method is compared to the state of the art, both for maximization and
minimization, a comparison which shows asymtotic surperiority in information
content per character written.


\bibliography{main}

\end{document}


