library("oai")

id('http://export.arxiv.org/oai2?verb=Identify')

records <- list_records(from = '2018-01-01T', until = '2020-12-31T')
# records <- list_records(from = '2018-01-01T', until = '2018-01-10T')

# colnames(records)

sink('Output2')

for (i in seq_along(records['title'])) {
    # print(paste(records['title'], records['creator']))
    print(paste(records['title'][[i]], '; ', records['creator'][[i]], ', ', records['creator.1'][[i]], '; ', records['datestamp'][[i]]))
}

# records['title']

# records['creator']
