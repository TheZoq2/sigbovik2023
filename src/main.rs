use std::collections::HashMap;

use regex::Regex;

const TEX_TEMPLATE: &'static str = r#"
"#;

#[derive(Debug)]
struct Paper {
    name: String,
    author1: String,
    author2: String,
    year: String,
    month: String,
}

impl std::fmt::Display for Paper {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            r#"\textbf{{{author}{author_extra}}}. {title}. {year}-{month}"#,
            author = self.author1,
            author_extra = (self.author2 != "NA")
                .then(|| " et.al")
                .unwrap_or_else(|| ""),
            title = self.name,
            year = self.year,
            month = self.month
        )
    }
}

impl<'a, T> From<T> for Paper
where
    T: Iterator<Item = String>,
{
    fn from(mut value: T) -> Self {
        Paper {
            name: value.next().unwrap(),
            author1: value.next().unwrap(),
            author2: value.next().unwrap(),
            year: value.next().unwrap(),
            month: value.next().unwrap(),
        }
    }
}

enum Item {
    Paper(Paper),
    Space,
    Period,
}

fn main() {
    [(
        Regex::new(r#"\[\d*\] "([^;]+);([^,]*),([^;]*);\s*(\d\d\d\d)-(\d\d)"#)
            .expect("Failed to parse regex"),
        std::fs::read_to_string("FullOutput").expect("Failed to open Output"),
        std::fs::read_to_string("Text.txt").expect("Failed to read text"),
    )]
    .into_iter()
    .map(|(re, file_content, text)| {
        (
            file_content
                .lines()
                .filter(|s| !s.is_empty())
                .filter_map(|line| re.captures(line))
                .map(|cap| {
                    [
                        cap.get(1).unwrap().as_str(),
                        cap.get(2).unwrap().as_str(),
                        cap.get(3).unwrap().as_str(),
                        cap.get(4).unwrap().as_str(),
                        cap.get(5).unwrap().as_str(),
                    ]
                    .into_iter()
                    .map(|v| v.trim())
                    .map(|v| v.replace("&", "\\&"))
                    .map(|v| v.replace("_", "\\_"))
                    .into()
                })
                .filter(|v: &Paper| v.author1 != "NA")
                .filter_map(|paper| {
                    paper
                        .author1
                        .to_lowercase()
                        .chars()
                        .next()
                        .map(|c| (c, paper))
                })
                .fold(HashMap::new(), |mut map, (first_char, paper)| {
                    map.entry(first_char).or_insert(vec![]).push(paper);
                    map
                }),
            text,
        )
    })
    .flat_map(|(mut char_map, text)| {
        text.trim()
            .to_lowercase()
            .chars()
            .map(|c| {
                if c == ' ' {
                    Item::Space
                } else if c == '.' {
                    Item::Period
                } else {
                    Item::Paper(
                        char_map
                            .get_mut(&c)
                            .expect(&format!("Had no {c}"))
                            .pop()
                            .expect(&format!("Ran out of {c}")),
                    )
                }
            })
            .collect::<Vec<_>>()
    })
    .fold((0, vec![]), |(i, lines), item| {
        (
            match &item {
                Item::Period => i,
                Item::Space => i,
                Item::Paper(_) => i + 1,
            },
            lines
                .into_iter()
                .chain(
                    match &item {
                        Item::Paper(p) => {
                            vec![format!(""), format!(""), format!("\\noindent[{}] {p}", i)]
                        }
                        Item::Period => vec![format!(r"\vspace{{2.2em}}")],
                        Item::Space => vec![format!(r"\vspace{{1.0em}}")],
                    }
                    .into_iter(),
                )
                .collect(),
        )
    })
    .1
    .into_iter()
    .for_each(|v| println!("{v}"))
}
